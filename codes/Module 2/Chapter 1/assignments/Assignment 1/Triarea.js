function area() {
    let t11 = document.getElementById("t11");
    let t12 = document.getElementById("t12");
    let t21 = document.getElementById("t21");
    let t22 = document.getElementById("t22"); //fetching the input value from the user
    let t31 = document.getElementById("t31");
    let t32 = document.getElementById("t32");
    let answer = document.getElementById("answer");
    var x1 = parseFloat(t11.value);
    var y1 = parseFloat(t12.value);
    var x2 = parseFloat(t21.value);
    var y2 = parseFloat(t22.value);
    var x3 = parseFloat(t31.value);
    var y3 = parseFloat(t32.value); //convedrting the string value into float type
    console.log(y3);
    if (x1 == NaN || x2 == NaN || x3 == NaN || y1 == NaN || y2 == NaN || y3 == NaN) {
        alert("Only Integers value accepted");
    }
    else {
        var a = Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2)); //Calculating the side of the triangle
        console.log(a);
        var b = Math.sqrt(Math.pow((x1 - x3), 2) + Math.pow((y1 - y3), 2));
        console.log(b);
        var c = Math.sqrt(Math.pow((x3 - x2), 2) + Math.pow((y3 - y2), 2));
        console.log(c);
        var s = (a + b + c) / 2;
        var area = Math.sqrt(s * (s - a) * (s - b) * (s - c));
        answer.innerHTML = "the area is: </br>" + area.toString(); //printing the area of triangle
    }
}
//# sourceMappingURL=Triarea.js.map