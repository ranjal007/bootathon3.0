function area(){
    let t11:HTMLInputElement=<HTMLInputElement>document.getElementById("t11"); //getting value entered by the user

    let answer:HTMLParagraphElement=<HTMLParagraphElement>document.getElementById("answer");

    var r:number=parseFloat(t11.value);  // converting the string type to float type

    var s= (Math.PI * Math.pow(r,2));  //calculating the area

    answer.innerHTML="the area is: </br>"+ s.toString();    //printing the area
}