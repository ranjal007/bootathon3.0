var n1 = document.getElementById("n1");
var n2 = document.getElementById("n2");
var n3 = document.getElementById("n3");
var r1 = document.getElementById("r1"); // getting the values inpput by the user
function check() {
    if (n1.value.length < 1) {
        r1.value = "Error in Num 1"; //chechking wether the field is empty or not
    }
    else if (n2.value.length < 1) {
        r1.value = "Error in Num 2";
    }
    else {
        return true;
    }
}
function checkes() {
    if (n3.value.length < 1) {
        return true;
    }
    else if ((n1.value.length < 1) && (n2.value.length < 1)) {
        r1.value = "Error in Num";
    }
    else {
        return true;
    }
}
function checks() {
    if (n1.value.length < 1) {
        return true;
    }
    else if (n2.value.length < 1) {
        return true;
    }
    else if (n3.value.length < 1) {
        r1.value = "Error in Degree"; //checking wether the field of degree is empty or not
    }
    else {
        return true;
    }
}
function add() {
    r1.value = "";
    if (check()) {
        var c = parseFloat(n1.value) + parseFloat(n2.value);
        r1.value = c.toString();
    }
}
function sub() {
    r1.value = "";
    if (check()) {
        var d = parseFloat(n1.value) - parseFloat(n2.value);
        r1.value = d.toString();
    }
}
function mul() {
    r1.value = "";
    if (check()) {
        var d = parseFloat(n1.value) * parseFloat(n2.value);
        r1.value = d.toString();
    }
}
function div() {
    r1.value = "";
    if (check()) {
        var d = parseFloat(n1.value) / parseFloat(n2.value);
        r1.value = d.toString();
    }
}
function reset() {
    n1.value = "";
    n2.value = "";
    r1.value = "";
}
function sin() {
    r1.value = "";
    if (checks()) {
        var a = Math.PI / 180 * parseFloat(n3.value);
        var d = Math.sin(a);
        r1.value = d.toString();
    }
}
function cos() {
    r1.value = "";
    if (checks()) {
        var a = Math.PI / 180 * parseFloat(n3.value);
        var d = Math.cos(a);
        r1.value = d.toString();
    }
}
function tan() {
    r1.value = "";
    if (checks()) {
        var a = Math.PI / 180 * parseFloat(n3.value);
        var d = Math.tan(a);
        r1.value = d.toString();
    }
}
function sq() {
    r1.value = "";
    if (checkes()) {
        var a = parseFloat(n1.value);
        var d = Math.pow(a, 2);
        r1.value = d.toString();
    }
}
function sqr() {
    r1.value = "";
    if (checkes()) {
        var a = parseFloat(n1.value);
        var d = Math.sqrt(a);
        r1.value = d.toString();
    }
}
//# sourceMappingURL=app.js.map