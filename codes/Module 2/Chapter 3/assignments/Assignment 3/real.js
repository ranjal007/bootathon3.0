function check() {
    var a = document.getElementById("t1"); //getting the values input by the user
    var b = document.getElementById("t2"); //getting the values input by the user
    var c = document.getElementById("t3");
    var data = a.value;
    var real;
    var imaginary;
    var i = data.indexOf("+"); //checking the index of +
    var j = data.indexOf("-"); //checking the index of -
    console.log(i);
    if ((i != -1)) {
        real = +data.substring(0, i); //calculating the real part of complex number
        console.log(real);
        imaginary = +data.substring(i, data.length - 1); //calculating the imaginary part of the complex number
        console.log(imaginary);
        b.value = "Real part  :" + real;
        c.value = "Imaginary part  :" + imaginary;
    }
    else if ((j != -1)) {
        real = +data.substring(0, j);
        console.log(real);
        imaginary = +data.substring(j, data.length - 1);
        console.log(imaginary);
        b.value = "Real part  :" + real;
        c.value = "Imaginary part  :" + imaginary;
    }
    else {
        var k = data.indexOf("i");
        if (k != -1) {
            imaginary = +data.substring(0, data.length - 1);
            b.value = "Real part  : 0";
            c.value = "Imaginary part  :" + imaginary;
        }
        else {
            real = +data.substring(0, data.length);
            b.value = "Real part  :" + real;
            c.value = "Imaginary part  : 0";
        }
    }
}
//# sourceMappingURL=real.js.map