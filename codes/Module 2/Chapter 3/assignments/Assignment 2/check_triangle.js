function check() {
    var x = document.getElementById("t1"); //getting the side of a triangle
    var y = document.getElementById("t2");
    var z = document.getElementById("t3");
    var a = +x.value; //converting string into number type
    var b = +y.value;
    var c = +z.value;
    if (a == b && b == c) { //condition to check if its a equilateral triangle
        document.getElementById("p1").innerHTML = " The triangle is Equilateral";
    }
    else if ((a == b && a != c) || (b == c && b != a) || (a == c && c != b)) { //condition to check if its a isocelles triangle
        document.getElementById("p1").innerHTML = "The triangle is iscocelles";
        if (Math.pow(a, 2) == (Math.pow(b, 2) + Math.pow(c, 2)) || Math.pow(b, 2) == (Math.pow(a, 2) + Math.pow(c, 2)) || Math.pow(c, 2) == (Math.pow(b, 2) + Math.pow(a, 2))) //checing if the iscocelles triangle is right angle triangle or not
            document.getElementById("p2").innerHTML = "The triangle is also a right angle triangle";
    }
    else if (a != b && b != c && a != c) { //checking wether the triangle is scalene or not
        document.getElementById("p1").innerHTML = "The triangle is scalene";
        if (Math.pow(a, 2) == (Math.pow(b, 2) + Math.pow(c, 2)) || Math.pow(b, 2) == (Math.pow(a, 2) + Math.pow(c, 2)) || Math.pow(c, 2) == (Math.pow(b, 2) + Math.pow(a, 2)))
            document.getElementById("p2").innerHTML = "The triangle is also a right angle triangle";
    }
}
//# sourceMappingURL=check_triangle.js.map