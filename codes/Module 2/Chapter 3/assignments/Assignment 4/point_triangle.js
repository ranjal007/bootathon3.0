function check() {
    var x = document.getElementById("t1"); //x1
    var y = document.getElementById("t2"); //y1
    var z = document.getElementById("t3"); //x2
    var r = document.getElementById("t4"); //y2
    var s = document.getElementById("t5"); //x3
    var t = document.getElementById("t6"); //y3
    var u = document.getElementById("t7"); //x
    var v = document.getElementById("t8"); //y
    var a = +x.value; //x1
    var b = +y.value; //y1
    var c = +z.value; //x2
    var d = +r.value; //y2
    var e = +s.value; //x3
    var f = +t.value; //y3
    var g = +u.value; //x
    var h = +v.value; //y
    var area = Math.abs((a * (d - f) + c * (f - b) + e * (b - d)) / 2); //calculating the area of whole triangle
    var area1 = Math.abs((g * (b - d) + a * (d - h) + c * (h - b)) / 2); //calculating the area of first part of that triangle
    var area2 = Math.abs((a * (f - b) + e * (b - h) + c * (h - f)) / 2); //calculating the area of second part of that triangle
    var area3 = Math.abs((g * (d - f) + c * (f - h) + e * (h - d)) / 2); //calculating the area of third part of that triangle
    var sum = area1 + area2 + area3;
    if (Math.abs(area - sum) < 0.000001) //checking the codition wether the point lies outside or inside of the triangle
        document.getElementById("p1").innerHTML = "The point lies inside the triangle";
    else
        document.getElementById("p1").innerHTML = "The point lies outside the triangle";
}
//# sourceMappingURL=point_triangle.js.map