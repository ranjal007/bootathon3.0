

// Function to Convert to UpperCase
function convertUpper() {

    var r1: HTMLInputElement = <HTMLInputElement>document.getElementById("string");
    var para1: HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("uppercase");
    var values: string = r1.value; //storing the value input by the user

    var substring = values.toUpperCase(); //converting the string to UpperCase
    console.log(values);
    para1.innerHTML = "String converted to Upper Case is:" + substring;
}

// Function to Convert to LowerCase
function convertLower() {

    var r1: HTMLInputElement = <HTMLInputElement>document.getElementById("string");
    var para2: HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("lowercase");
    var values: string = r1.value;

    var substring = values.toLowerCase();
    console.log(values);
    para2.innerHTML = "String converted to Lower Case is:" + substring.toString();
}


// Function to Split all the words in the string
function splitWords() {

    var r1: HTMLInputElement = <HTMLInputElement>document.getElementById("string");
    var para2: HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("splitwords");
    var values: string = r1.value;

    var array = values.split(" "); // Splitting String with " " space as a delimeter
    console.log(values);
    para2.innerHTML = "String splitted is:-:";
    for (let index = 0; index < array.length; index++) {
        para2.innerHTML += "</br>"+array[index];
    }


}

